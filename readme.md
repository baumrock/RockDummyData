# RockDummyData

With this module you can easily create all kinds of custom dummy-data for testing your modules and applications.

1. add data
    + just add a txt file to the /data folder of this module, eg "/data/sampledata.txt"
    + put each value on one separate line of the file
    + access your value via $dummy->sampledata (always the same as the filename)

1. examples

very simple example printing 15 date strings:
```php
$rdd = $modules->get('RockDummyData');
for($i=0; $i<15; $i++) {
    // this has to be inside the for-loop to always get a new dummy
    $dummy = $rdd->getDummy();
    echo date("d.m.Y H:i:s", $dummy->timestamp) . "<br>";
}
```

more advanced (but still dead simple) example returning a json with dummy data
```php
$json = new stdClass();
$json->data = array();
$rdd = $modules->get('RockDummyData');
for($i=0; $i<3000; $i++) {
    // this has to be inside the for-loop to always get a new dummy
    $dummy = $rdd->getDummy();
    $obj = new stdClass();
    $obj->name = $dummy->forename . ' ' . $dummy->surname;
    $obj->position = $dummy->job;
    $obj->office = $dummy->city;
    $obj->color = $dummy->color;
    $obj->start_date = new stdClass();
        $obj->start_date->display = date('d.m.Y',$dummy->timestamp);
        $obj->start_date->sort = $dummy->timestamp;
    $obj->salary = rand(0,10000);
    $json->data[] = $obj;
}
echo json_encode($json);
```
