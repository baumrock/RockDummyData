<?php namespace ProcessWire;
/**
 * Bernhard Baumrock, baumrock.com
 */
class RockDummyData extends WireData implements Module {

    public $types = [];
    private $dataarray = [];

    /**
     * getModuleInfo is a module required by all modules to tell ProcessWire about them
     *
     * @return array
     *
     */
    public static function getModuleInfo() {
        return array(
            'title' => 'RockDummyData',
            'version' => 001,
            'summary' => 'Easily create dummy-data to test your modules.',
            'singular' => true,
            'autoload' => false,
            'icon' => 'bolt',
            'author' => 'Bernhard Baumrock, baumrock.com',
            );
    }

    /**
     * Initialize the module
     */
    public function init() {
    }

    /**
     * load data of txt files in data folder into memory
     */
    private function loadData($datatypes) {
        $dir = new \DirectoryIterator(__DIR__.'/data');
        foreach ($dir as $fileinfo) {
            if($fileinfo->isDot()) continue;
            if($fileinfo->getExtension() != 'txt') continue;

            // get the filename
            $name = $fileinfo->getBasename('.txt');

            // if datatype array is set only load those files
            if(count($datatypes)) {
                if(!in_array($name, $datatypes)) continue;
            }

            // load every line of the file into the array
            $data = file($fileinfo->getPathname(), FILE_IGNORE_NEW_LINES);

            // save the array to the dataarray of this instance
            $this->dataarray[$name] = $data;
        }
    }

    /** 
     * return an object with new dummy data
     */
    public function getDummy($datatypes = []) {

        // load data if it is not already loaded
        if(!count($this->dataarray)) $this->loadData($datatypes);

        // create dummy object and populate all properties
        // with a random entry of the data
        $dummy = new \stdClass();
        foreach($this->dataarray as $name => $data) {
            $dummy->{$name} = $data[rand(0,count($data)-1)];
        }
        return $dummy;
    }
}
